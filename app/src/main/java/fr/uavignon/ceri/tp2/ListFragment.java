package fr.uavignon.ceri.tp2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;
import java.util.Locale;

import fr.uavignon.ceri.tp2.data.Book;

import static fr.uavignon.ceri.tp2.data.Book.books;

public class ListFragment extends Fragment {

    private ListViewModel viewModel;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerAdapter adapter;
    private TextView bookId;
    private EditText booktitle;
    private EditText bookauthors;
    private EditText bookyear;
    private EditText bookgenres;
    private EditText bookpublisher;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(ListViewModel.class);

        recyclerView = view.findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter();
        recyclerView.setAdapter(adapter);

        observerSetup();

        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                ListFragmentDirections.ActionFirstFragmentToSecondFragment action = ListFragmentDirections.actionFirstFragmentToSecondFragment();
                action.setBookNum(-1);
                Navigation.findNavController(view).navigate(action);
            }
        });
    }

    private void observerSetup(){
        viewModel.getAllBooks().observe(getViewLifecycleOwner(),
                new Observer<List<Book>>() {
                    @Override
                    public void onChanged(List<Book> books)
                    {
                        adapter.setBookList(books);
                    }
        });

        //????
        viewModel.getSearchResults().observe(getViewLifecycleOwner(),
                new Observer<List<Book>>() {
                    @Override
                    public void onChanged(@Nullable final List<Book> products) {

                        if (products.size() > 0) {
                            bookId.setText(String.format(Locale.US, "%d",
                                    products.get(0).getId()));
                            booktitle.setText(products.get(0).getTitle());
                            bookauthors.setText(products.get(0).getAuthors());
                            bookyear.setText(products.get(0).getYear());
                            bookgenres.setText(products.get(0).getGenres());
                            bookpublisher.setText(products.get(0).getPublisher());
                        } else {
                            bookId.setText("Aucune correspondance");
                        }
                    }
                });
    }

    private void listenerSetup() {
        // Button buttonBack = getView().findViewById(R.id.buttonBack);  //utile?
        Button buttonUpdate = getView().findViewById(R.id.buttonUpdate);

        //Pour update mon livre
        buttonUpdate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                String title = booktitle.getText().toString();
                String authors = bookauthors.getText().toString();
                String year = bookyear.getText().toString();
                String genres = bookgenres.getText().toString();
                String publisher = bookpublisher.getText().toString();

                if (!title.equals("") && !authors.equals("") && !year.equals("") && !genres.equals("") && !publisher.equals("")) {
                    Book book = new Book(title,authors,year,genres,publisher);
                    viewModel.updateBook(book);
                } else {
                    bookId.setText("Incomplete information");
                }
            }
        });
    }
}