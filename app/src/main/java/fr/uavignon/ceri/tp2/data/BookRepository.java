package fr.uavignon.ceri.tp2.data;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;


import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static fr.uavignon.ceri.tp2.data.BookRoomDatabase.databaseWriteExecutor;

public class BookRepository {

    private LiveData<List<Book>> allBooks;
    private MutableLiveData<Book> selectedBook = new MutableLiveData<Book>();
    private MutableLiveData<List<Book>> searchResults = new MutableLiveData<>();
    private BookDao bookDao;

    public BookRepository(Application application)
    {
        BookRoomDatabase db = BookRoomDatabase.getDatabase(application);
        bookDao = db.bookDao();
        allBooks = bookDao.getAllBooks();
    }
    public void updateBook(Book book)
    {
        databaseWriteExecutor.execute(() -> { bookDao.updateBook(book);});
    }

    public void insertBook(Book book)
    {
        databaseWriteExecutor.execute(() -> { bookDao.insertBook(book);});
    }

    public void getBook(long id)
    {
        Future<Book> fbook = databaseWriteExecutor.submit(() -> {
            return bookDao.getBook(id);
        });
        try {
            selectedBook.setValue(fbook.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void deleteBook(long id)
    {
        databaseWriteExecutor.execute(() -> { bookDao.deleteBook(id); });
    }

    public LiveData<List<Book>> getAllBooks() { return allBooks; }

    public MutableLiveData<List<Book>> getSearchResults() {
        return searchResults;
    }

    public MutableLiveData<Book> getSelectedBook() {
        return selectedBook;
    }

    public void findBook(long id) {

        Future<Book> fproducts = databaseWriteExecutor.submit(() -> {
            return bookDao.getBook(id);
        });
        try {
            selectedBook.setValue(fproducts.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
